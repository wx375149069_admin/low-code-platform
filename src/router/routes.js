/**
 * 路由目录创建使用：
 * 使用包：vite-plugin-pages
 * npm地址：https://www.npmjs.com/package/vite-plugin-pages
 */
import login from "@/view/login.vue";
import app from "@/layouts/app.vue";
// import preview from "@/view/preview.vue";
import project from "@/view/project.vue";
/**
 * () => import('@/view/login.vue')
 * () => import('@/layouts/app.vue')
 * () => import('@/view/preview.vue')
*/
// 单个页面显示：登陆页面、错误页面等
const fameOutPage = [
    {
        path: '/login',
        name: 'login',
        meta:{
            auth: false,
            title:"登陆"
        },
        component: login
    },
    {
        path: '/panel',
        name: 'panel',
        meta:{
            auth: true,
            title:"面板"
        },
        component: app
    },
    {
        path: '/',
        name: 'project',
        meta:{
            auth: true,
            title:"project"
        },
        component: project
    }
]

// 重新组织后导出
export default [
    ...fameOutPage
]
