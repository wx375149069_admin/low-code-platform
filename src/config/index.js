import devEnv from './env.dev'
import stagingEnv from './env.staging'
import prodEnv from './env.prod'

// 根据环境引入不同配置
const EnvMap = {
    dev: devEnv,
    staging: stagingEnv,
    prod: prodEnv
}
console.log('[运行环境]', import.meta.env.MODE)
const env = import.meta.env.MODE || 'dev'
export default EnvMap[env] || EnvMap["prod"]
