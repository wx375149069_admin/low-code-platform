import { defineStore } from 'pinia'

const useRoute = defineStore('routeConfig', {
    state: () => ({
        routeConfig: [],
        nowItem : {}
    }),
    actions: {
        saveRouteConfig(params){
            this.routeConfig = params;
        },
        saveNowItem(params){
            this.nowItem = params;
        },
    },
    persist: true,
})

export default useRoute