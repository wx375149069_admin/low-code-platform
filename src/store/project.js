import { defineStore } from 'pinia'

const useProject = defineStore('projectConfig', {
    state: () => ({
        nowProject : {},
        isEditModel:true,
    }),
    actions: {
        saveNowProjectItem(params){
            this.nowProject = params;
        },
        setPreviewModel(params){
            this.isEditModel = params;
        }
    },
    persist: true,
})

export default useProject