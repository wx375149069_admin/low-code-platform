//是否有日期时间选择器判断
function isHasDateTimePicker(array){
    let isHasDateTimePicker = false
    for(let j =0; j< array.length;j++){
        let itemInfo = array[j];
        if(itemInfo.type == 'datetimerange'){
            isHasDateTimePicker = true;
            break
        }
    }
    return isHasDateTimePicker;
}
//判断是否有长label
function isLongLabel(array){
    let isHasLongLabel = false;
    for(let i =0; i< array.length;i++){
        let itemInfo = array[i];
        if(itemInfo.label.split('：')[0].length >= 6){
            isHasLongLabel = true;
            break
        }
    }
    return isHasLongLabel;
}

//格式化日期选择器类型
function forMatDatePicker(type){
    let forMatType = type;
    return forMatType.split('Picker')[0].toLowerCase();
}

function returnBrack(params){
    return `{{${params}}}`
}

export { 
    isHasDateTimePicker,
    isLongLabel,
    forMatDatePicker,
    returnBrack
}