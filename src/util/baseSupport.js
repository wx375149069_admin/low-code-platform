//寻找路由中首页
let findHomePageName = (route) => {
    let hasHomePage = false;
    let homePageItem = {};
    route.forEach(item => {
        if(typeof item.homePage != "undefined" && item.homePage){
            hasHomePage = true;
            homePageItem = item;
        }
    })
    if(hasHomePage){
        return homePageItem
    }else{
        return {};
    }
}
export{
    findHomePageName
}