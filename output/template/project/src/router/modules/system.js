export default [{
    path:"/product",//注意 path 路径必须得有，并且不可重复，每一个都不一样
    meta:{
        title:"产品管理"
    },
    sideBarIconName:"location",//侧边栏图标名称-暂不使用
    sideBarName:"一级目录",//侧边栏显示名称
    sideBarShow:true,//是否在侧边栏显示
    auth:"admin",//菜单权限
    secMenu:[
        {
            name:"second",//如果有name的话，不允许重复
            path:"/product/second",
            component:() => import('@/view/customer/index.vue'),
            meta:{
                title:"二级目录"
            },
            sideBarIconName:"present",//侧边栏图标名称-暂不使用
            sideBarName:"二级目录",//侧边栏显示名称
            sideBarShow:true,//是否在侧边栏显示
            auth:"admin",//菜单权限
            children:[]
        },
        {
            name:"keep",//如果有name的话，不允许重复
            path:"/product/keep",
            component:() => import('@/view/customer/keep.vue'),
            meta:{
                title:"二级目录"
            },
            sideBarIconName:"present",//侧边栏图标名称-暂不使用
            sideBarName:"keep",//侧边栏显示名称
            sideBarShow:true,//是否在侧边栏显示
            auth:"admin",//菜单权限
            children:[]
        },
        {
            name:"secondAdd",//如果有name的话，不允许重复
            path:"/product/secondAdd",
            detailName:"详情",
            component:() => import('@/view/customer/detail.vue'),
            meta:{
                title:"详情"
            },
            sideBarShow:false,//是否在侧边栏显示
            auth:"admin",//菜单权限
            children:[],
        }
    ],
}]