import AppLayout from '@/layouts/app.vue'
import { treeToArray } from "@/util/tree.js"
import routeConfig from "./routerConfig.js";

//设置进入页面是否需要登陆验证
const meta = { auth: true }

//设置首页
const indexPage = {
    path: '/index',
    name: 'index',
    meta:{
        auth: true,
        title:"首页"
    },
    component: () => import('@/view/index.vue')
}
routeConfig.push(indexPage)

export default [
    {
        path: '/',
        component: AppLayout,
        meta,
        redirect: () => {
            return { path:"index" }
        },
        children: treeToArray(routeConfig)
    }
]
