//cookie
/**
  //*********可设置失效时间，没有设置的话，默认是关闭浏览器后失效*********
  document.cookie = 'token' + "=" + "123"; 
*/
//*********获取指定名称的cookie值*********
function getCookie(name) {
  // (^| )name=([^;]*)(;|$),match[0]为与整个正则表达式匹配的字符串，match[i]为正则表达式捕获数组相匹配的数组；
  var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
  if(arr != null) {
    return unescape(arr[2]);
  }
  return null;
}
//*********删除cookie （给某个键值对设置过期的时间）*********
function deleteCookie(name){
  var exp = new Date();
  exp.setTime(exp.getTime() - 1);
  var cval = getCookie(name);
  if(cval != null) {
      document.cookie= name + "=" + cval + ";expires=" + exp.toGMTString();
  }
}
//Storage
/**
//localStorage和sessionStorage所使用的方法是一样的
//Storage 是以字符串保存数据的，所以在保存 JSON 格式的数据之前，需要把 JSON 格式的数据转化为字符串，这个操作叫序列化
//*********存储数据*********
sessionStorage.setItem(name,num);
//*********获取全部数据*********
let dataAll=sessionStorage.valueOf();
//*********获取指定键名数据*********
var dataSession=sessionStorage.getItem(name);
var dataSession2=sessionStorage.sessionData;//sessionStorage是js对象，也可以使用key的方式来获取值
//*********删除指定键名数据*********
sessionStorage.removeItem(name);
//*********清空缓存数据*********
sessionStorage.clear();
*/
export {
  getCookie,
  deleteCookie
}
