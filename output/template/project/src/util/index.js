
import * as time from './time.js';
import * as common from './common.js';
import * as cookie from './cookie.js';
import * as tree from './tree.js';

var allObject = {
    ...cookie,
    ...common,
    ...time,
    ...tree
};
export {allObject};
