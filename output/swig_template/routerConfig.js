{% for key,item in pageConfig -%}
import {{item}} from "./modules/{{item}}.js";
{% endfor -%}

export default [
    {% for key,item in pageConfig -%}
    ...{{item}},
    {% endfor -%}
]
