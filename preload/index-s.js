import { contextBridge,ipcRenderer } from "electron";
import { join } from "path";
const fs = require('fs-extra');
const swig = require('swig');
swig.setDefaults({ cache: false });
contextBridge.exposeInMainWorld("preload", {
    node: () => process.versions.node,
    chrome: () => process.versions.chrome,
    electron: () => process.versions.electron,
    openChildWindow: async () => await ipcRenderer.invoke('open-child-window'),
    selectOutputPath: async () => await ipcRenderer.invoke('select-output-path'),
    dirname:join(__dirname,'../../../'),
    path_join:join,
    fs:fs,
    swig:swig
});